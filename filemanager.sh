#!/bin/bash
operation=$1
location=$2
dirname=$3
fileNcontent=$4
startrange=$5
endrange=$6

set -ex

function addDir(){

mkdir -p $location/$dirname
	echo "directory is created"


}


function listFiles(){

	ls -l $location | grep -v '^d'
	echo "all files  are listed" 


}



function listDirs(){

	ls -l $location | grep '^d'
	echo "all the dirextories are listed"
}

function listAll(){

	ls -lA $location
        echo "All Files and Directoires are listed"
}


function deleteDir(){

	echo "do you want to del file (y/n?"
	read y

	if [ $y == "y" ]
	then

	rmdir  $location/$dirname
	echo "directory is removed"

else echo "file not deleted"
fi
}


function addFile(){

	cd $location/$dirname 
	touch -f $fileNcontent

	echo "file is created"
}

function addContentToFile(){
	
	echo "$fileNcontent" >> $location/$dirname
        echo " content added to end of file"

}

function addContentToFileBeginning(){

	#echo -e "task goes here\n$(cat todo.txt)" > todo.txt

	echo -e "$fileNcontent\n$(cat $location/$dirname)" > $location/$dirname
  # echo -e<<< "$startrange 
#	> $(< $location/$dirname/fileNcontent)"
 #sed -i "1s/^/ $1\n / " $filename
#  cat <<< "$location
 #  $(< $dirname)" > $fileNcontent 
}


function showFileBeginningContent(){

	head -n $startrange $location/$dirname/$fileNcontent

	echo "here is your file's beginning content"

}


function showFileEndContent(){

	tail -n $startrange $location/$dirname/$fileNcontent

	echo "here is your file's end content"



}


function moveFile(){
	mv $location/$dirname/$fileNcontent $startrange/$endrange

	echo "file is moved  to your given location GO and check "

}


function copyFile(){
	cp $location/$dirname/$fileNcontent $startrange/$endrange

	echo "file is copied to  your given location.Go and Check"

}


function clearFileContent(){
	: > $location/$dirname/$fileNcontent

	echo "content of the file is cleared"

}

function deleteFile(){

	rm $location/$dirname/$fileNcontent

	echo "directory is deleted"


}





 
case $operation in
	addDir)
		addDir
		;;
	listFiles)
		listFiles
		;;
	listDirs)
		listDirs
		;;
	listAll)
		listAll
		;;
	deleteDir)
		deleteDir
		;;
	addFile)
		addFile
		;;
	addContentToFile)
		addContentToFile
		;;
	addContentToFileBeginning)
		addContentToFileBeginning
		;;
	showFileBeginningContent)
		showFileBeginningContent
		;;
	showFileEndContent)
		showFileEndContent
		;;
	showFileContentAtLine)
		showFileContentAtLine
		;;
	showFileContentForLineRange)
		showFileContentForLineRange
		;;
	moveFile)
		moveFile
		;;
	copyFile)
		copyFile
		;;
	clearFileContent)
		clearFileContent
		;;
	deleteFile)
		deleteFile
		;;
	
	*)
		echo "please enter valid function"
		;;
  esac

















